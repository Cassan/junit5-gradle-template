# JUnit5 Gradle Template

Basic JUnit 5 Gradle Template

## Cloning the repository

    git clone https://gitlab.com/qalabs/blog/junit5-gradle-template.git
    
## Run the tests

    gradlew clean test

